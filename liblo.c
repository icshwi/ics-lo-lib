#include "liblo.h"
#include <unistd.h>
#include <math.h>
#include "xilspi.h"

#define SPI_BASE_ADDRESS	0xC01000
#define LO_RTM_FIRMWARE_ID	0x101

enum LORegs {
	REG_CONFIG = 0,
	REG_ATTEN = 1,
	REG_CLK_POW_LO = 2,
	REG_CLK_POW_HI = 3,
	REG_LO_POW_LO = 4,
	REG_LO_POW_HI = 5,
	REG_REF_POW_LO = 6,
	REG_REF_POW_HI = 7,
};

//////////////////////////////////////////////////////////////////////////////
// Board initialization
//////////////////////////////////////////////////////////////////////////////

int lo_init(xildev *dev)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	uint32_t reg_val;
	int result = xil_read_reg(dev, 0x10, &reg_val);
	if(result != LO_ERROR_NONE)
		return result;
	if(reg_val != LO_RTM_FIRMWARE_ID)
		return LO_WRONG_FIRMWARE_ID;
	if(xspi_init(dev, SPI_BASE_ADDRESS, XSPI_COMMON_FLAGS) != 0)
		return LO_ERROR_REG_ACCESS;
	return LO_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Getting error messages
//////////////////////////////////////////////////////////////////////////////

const char * lo_error_string(int error_code)
{
	switch(error_code)
	{
	case LO_ERROR_NONE:				return "No error reported";
	case LO_ERROR_REG_ACCESS:		return "Register access error";
	case LO_ERROR_NULL_POINTER:		return "Null pointer passed to a function";
	case LO_ERROR_OUT_OF_RANGE:		return "Parameter out of range";
	case LO_ERROR_NO_RESPONSE:		return "No response from the device";
	case LO_WRONG_FIRMWARE_ID:		return "Unexpected value of firmware ID register";
	default:						return "Unknown error code";
	}
}

//////////////////////////////////////////////////////////////////////////////
// SPI Communication
//////////////////////////////////////////////////////////////////////////////

int lo_reg_read(xildev *dev, uint8_t reg, uint8_t *value)
{
	if(!dev || !value)
		return LO_ERROR_NULL_POINTER;
	uint8_t frame[2];
	frame[0] = ((reg << 4) & 0xF0) | 1;
	frame[1] = 0xFF;
	xspi_select_slave(dev, SPI_BASE_ADDRESS, 0);
	int res = xspi_exchange(dev, SPI_BASE_ADDRESS, frame, frame, sizeof(frame));
	xspi_deselect_slave(dev, SPI_BASE_ADDRESS);
	*value = frame[1];
	return res;
}

int lo_reg_write(xildev *dev, uint8_t reg, uint8_t value)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	uint8_t frame[2];
	frame[0] = ((reg << 4) & 0xF0) | 0;
	frame[1] = value;
	xspi_select_slave(dev, SPI_BASE_ADDRESS, 0);
	int res = xspi_exchange(dev, SPI_BASE_ADDRESS, frame, NULL, sizeof(frame));
	xspi_deselect_slave(dev, SPI_BASE_ADDRESS);
	return res;
}

//////////////////////////////////////////////////////////////////////////////
// Registers access
//////////////////////////////////////////////////////////////////////////////

int lo_config_set_bits(xildev *dev, uint8_t set_mask)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	uint8_t reg_val;
	int result = lo_reg_read(dev, REG_CONFIG, &reg_val);
	if(result != LO_ERROR_NONE)
		return result;
	reg_val |= set_mask;
	return lo_reg_write(dev, REG_CONFIG, reg_val);
}

int lo_config_clear_bits(xildev *dev, uint8_t clear_mask)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	uint8_t reg_val;
	int result = lo_reg_read(dev, REG_CONFIG, &reg_val);
	if(result != LO_ERROR_NONE)
		return result;
	reg_val &= ~clear_mask;
	return lo_reg_write(dev, REG_CONFIG, reg_val);
}

int lo_config_get(xildev *dev, uint8_t *value)
{
	if(!dev || !value)
		return LO_ERROR_NULL_POINTER;
	return lo_reg_read(dev, REG_CONFIG, value);
}

int lo_atten_set(xildev *dev, uint8_t value)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	if(value > 0x1F)
		return LO_ERROR_OUT_OF_RANGE;
	return lo_reg_write(dev, REG_ATTEN, value);
}

int lo_atten_get(xildev *dev, uint8_t *value)
{
	if(!dev || !value)
		return LO_ERROR_NULL_POINTER;
	uint8_t reg_val;
	int result = lo_reg_read(dev, REG_ATTEN, &reg_val);
	if(result != LO_ERROR_NONE)
		return result;
	*value = reg_val & 0x1F;
	return LO_ERROR_NONE;
}

int lo_power_get(xildev *dev, uint16_t *clk_pow, uint16_t *lo_pow, uint16_t *ref_pow)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	int result;
	uint8_t val_lo, val_hi;
	if(clk_pow)
	{
		result = lo_reg_read(dev, REG_CLK_POW_LO, &val_lo);
		if(result != LO_ERROR_NONE)
			return result;
		result = lo_reg_read(dev, REG_CLK_POW_HI, &val_hi);
		if(result != LO_ERROR_NONE)
			return result;
		*clk_pow = ((val_hi << 8) | val_lo) & 0x0FFF;
	}
	if(lo_pow)
	{
		result = lo_reg_read(dev, REG_LO_POW_LO, &val_lo);
		if(result != LO_ERROR_NONE)
			return result;
		result = lo_reg_read(dev, REG_LO_POW_HI, &val_hi);
		if(result != LO_ERROR_NONE)
			return result;
		*lo_pow = ((val_hi << 8) | val_lo) & 0x0FFF;
	}
	if(ref_pow)
	{
		result = lo_reg_read(dev, REG_REF_POW_LO, &val_lo);
		if(result != LO_ERROR_NONE)
			return result;
		result = lo_reg_read(dev, REG_REF_POW_HI, &val_hi);
		if(result != LO_ERROR_NONE)
			return result;
		*ref_pow = ((val_hi << 8) | val_lo) & 0x0FFF;
	}
	return LO_ERROR_NONE;
}

int lo_version_get(xildev *dev, uint16_t *ver)
{
	if(!dev || !ver)
		return LO_ERROR_NULL_POINTER;
	int result;
	uint8_t val_lo, val_med, val_hi;
	result = lo_reg_read(dev, REG_REF_POW_HI, &val_lo);
	if(result != LO_ERROR_NONE)
		return result;
	val_lo >>= 4;
	result = lo_reg_read(dev, REG_LO_POW_HI, &val_med);
	if(result != LO_ERROR_NONE)
		return result;
	val_med >>= 4;
	result = lo_reg_read(dev, REG_CLK_POW_HI, &val_hi);
	if(result != LO_ERROR_NONE)
		return result;
	val_hi >>= 4;
	*ver = (val_hi << 8) | (val_med << 4) | (val_lo << 0);
	return LO_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Value conversion
//////////////////////////////////////////////////////////////////////////////

float lo_power_in_dbm(uint16_t power_word)
{
	double adc_voltage = 2.5 * (double)power_word / 4096.0; // V
	double detector_voltage = adc_voltage / 2.0; // V
	double detector_offset = -17; // dBm
	double detector_sensitivity = 50.0; // dBm / V
	return (float)(detector_offset + detector_sensitivity * detector_voltage);
}
