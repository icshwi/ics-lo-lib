CCFLAGS := -Wall -O3 -std=gnu11 -g -ggdb
LDFLAGS := -Wall -O3

INCLUDES := . ../ics-xdriver-lib
TARGETS := liblo.a lo-tool
LIBS := -L. -llo -L../ics-xdriver-lib -lxildrv -lm
LIB_OBJ := liblo.o xilspi.o
APP_OBJ := lo-tool.o
HEADERS := $(wildcard *.h)

all: $(TARGETS)

clean:
	@$(RM) -f $(TARGETS) $(LIB_OBJ) $(APP_OBJ)

lo-tool: lo-tool.o liblo.a $(HEADERS)
	$(CC) $(LDFLAGS) $< $(LIBS) -o $@

liblo.so: $(LIB_OBJ) $(HEADERS)
	$(CC) $(LDFLAGS) -fPIC -shared $(LIB_OBJ) -o $@

liblo.a: $(LIB_OBJ) $(HEADERS)
	ar rc $@ $(LIB_OBJ)

%.o: %.cpp $(HEADERS)
	$(CC) -c $(CCFLAGS) $(INCLUDES:%=-I%) $< -o $@

%.o: %.c $(HEADERS)
	$(CC) -c $(CCFLAGS) $(INCLUDES:%=-I%) $< -o $@
